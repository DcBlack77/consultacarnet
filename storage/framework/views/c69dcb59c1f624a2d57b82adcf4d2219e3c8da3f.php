<?php $__env->startSection('content'); ?>

    
    <div class="container">
        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
            <div class="contenedor well-menu" data-tipo ="5">
                <img src="<?php echo e(url('public/img/1.png')); ?>"  class="loguito" alt="">
                <div class="well" data-tipo ="5">
                    <b>inicio</b><br/>  
                    Ultimas 5 Elecciones
                </div>
            </div>

            <div class="contenedor well-menu" data-tipo ="1">
                <img src="<?php echo e(url('public/img/1.png')); ?>"  class="loguito" alt="">
                <div class="well" data-tipo ="1">
                    <b>PRESIDENCIALES</b><br/>  
                    Elecciones Presidenciales
                </div>
            </div>
             
            <div class="contenedor well-menu"  data-tipo ="2">
                 <img src="<?php echo e(url('public/img/2.png')); ?>"  class="loguito" alt="">
                <div class="well" data-tipo ="2">
                    <b>REGIONALES </b><br/>
                    Elecciones para Gobernador
                </div>
            </div>   
            <div class="contenedor well-menu" data-tipo ="3">
                <img src="<?php echo e(url('public/img/3.png')); ?>"  class="loguito" alt=""> 
                <div class="well" data-tipo ="3">
                    <b>MUNICIPALES</b><br/>
                    Elecciones para Alcaldes
                </div>
            </div>
            <div class="contenedor well-menu" data-tipo ="4">
                <img src="<?php echo e(url('public/img/4.png')); ?>"  class="loguito" alt="">
                <div class="well" data-tipo ="4">
                    <b>OTRAS ELECCIONES</b><br/>
                    Parlamentarias, Referendum, otras
                </div>
            </div>
           
            <div class="portlet box blue">
                <div class="panel-heading panel-1">
                    <h3 class="panel-title"><center>TIPO DE GRÁFICO</center></h3>
                </div>
                <div class="panel-body  panel-body-1">
                    <div id="contenido">  
                        <p>Seleccione el tipo de gráfica que desea visualizar</p>
                        <div class="list-group ">
                        
                            <a href="#" class="list-group-item" id="tendencia">
                                <i class="fa fa-line-chart fa-2x" aria-hidden="true"></i> <b>TENDENCIA</b>
                            </a>
                            
                            <a href="#" class="list-group-item" id="torta">
                            <i class="fa fa-pie-chart fa-2x" aria-hidden="true"></i> <b>TORTA </b>
                            </a>    
                           
                        </div> 
                    </div>  
                </div>
            </div>   
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12 col-lg-8">    
            <div class="panel panel-success">
                <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                    <div class="pull-left">
                        <h3 class="panel-title">Resultados de Elecciones - <span class="tipo_elecciones"></span>  <span class="nombre_municipio"></span> - <span class="nombre_parroquia"></span> <i class="fa fa-bar-chart" aria-hidden="true"></i></h3>
                    </div>
                </div>
                <div class="panel-body"  > 
                    <div id="grafica_principal"></div>
                </div>
            </div>   
        </div>
    </div>
     
    <div class="container">
        
        <div class="col-md-3 col-xs-12 col-sm-6 col-lg-3">
            <center>
                <img src="<?php echo e(url('public/img/mapa.png')); ?>" alt="">
                
            </center>        
        </div>
       <div class="col-md-9 col-xs-9 col-sm-6 col-lg-9">
           <div class="panel panel-default panel-mapa"> 
                <div class="panel-body">
                    <center>
                        
                        <b><span id="poblacion"></span>&nbsp;Poblacion</b><br/>
                                       
                    </center> 
                </div>  
            </div>
        </div>
        
        <div class="col-md-3 col-xs-12 col-sm-6 col-lg-3">
           <div class="panel panel-default panel-mapa"> 
                <div class="panel-body">
                    <center>
                        
                        <b><span id="electores"></span>&nbsp;Electores </b><br/>
                        <b><span id="militantes"></span>&nbsp;Militantes</b><br/>
                       
                
                    </center>
                </div>    
            </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 col-lg-3">
            <div class="panel panel-default panel-mapa">

                <div class="panel-body">
                    <center>   
                        <b>11 Municipios</b><br/>
                        <b><span id="parroquias"></span>&nbsp;Parroquia</b>
                          
                    </center>
                </div>
            
            </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 col-lg-3">
            <div class="panel panel-default panel-mapa">

                <div class="panel-body">
                    <center> 

                        <b><span id="centros"></span>&nbsp; Centro Electorales </b><br/>
                        <b><span id="mesas"></span>&nbsp; Mesas Electorales </b><br/>
                    </center>
                </div>
            
            </div>
        </div>
    </div>
   
    
    <div class="container">
        <div class="panel panel-success">
            <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                <div class="pull-left">
                    <h3 class="panel-title">Resultados de Elecciones  <span class="nombre_municipio"></span> - <span class="nombre_parroquia"></span> <i class="fa fa-bar-chart" aria-hidden="true"></i></h3>
                </div> 
                <div class="pull-right">
                    <div class="col-md-4 col-xs-3 col-sm-3" >
                        <button  class="btn btn-info" id="comparar" data-toggle="modal" data-target="#tabla_comparacion" style="background: #BB1B1B;" title="Comparar">
                             <i class="fa fa-exchange" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="col-md-4 col-xs-3 col-sm-3"  >
                        <button  class="btn btn-info" style="background: #BB1B1B; display: none;" title="Regresar" id="regresar" >
                             <i class="fa fa-backward" aria-hidden="true"></i>
                        </button>
                        <input type="hidden" id="adonde" name="" value="">
                    </div>

                    <div class="col-md-4 col-xs-3 col-sm-3">
                        <button class="btn btn-info clickable filter" style="background: #BB1B1B;" id="bus"  data-toggle="tooltip" title="Filtrar Informacion de la Tabla" data-container="body">
                             <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-body" id="buscar"  style="display: none;">
                <input type="text" class="form-control" id="task-table-filter" data-action="filter" data-filters="#task-table" placeholder="Filtrar"/>
            </div>
            <div class="panel-body" style="overflow: scroll;">
                <table class="table table-striped table-hover table-bordered table-responsive" id="task-table">
                    <thead>
                        <tr>
                            <th><span id="municipio">MUNICIPIOS</span></th>
                            <th colspan="2" style="text-align: center;">REG. 2008</th>
                            <th colspan="2" style="text-align: center;">PAR. 2010</th>
                            <th colspan="2" style="text-align: center;">PRE. 2012</th>
                            <th colspan="2" style="text-align: center;">REG. 2012</th>
                            <th colspan="2" style="text-align: center;">PRE. 2013</th>
                            <th colspan="2" style="text-align: center;">PAR. 2015</th>
                            <th colspan="2" style="text-align: center;">REG. 2017</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Resul</th>
                            <th>D.V</th>
                            
                            <th>Resul</th>
                            <th>D.V</th>
                            
                            <th>Resul</th>
                            <th>D.V</th>
                            
                            <th>Resul</th>
                            <th>D.V</th>
                            
                            <th>Resul</th>
                            <th>D.V</th>
                            
                            <th>Resul</th>
                            <th>D.V</th>

                            <th>Resul</th>
                            <th>D.V</th>
                        </tr>
                    </thead>
                    <tbody id="listado_municipios" style="font-size: 11px;">
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document"  style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        <span class="nombre_centro"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-5 col-xs-10 col-sm-10 col-lg-5">
                                <div class="panel panel-danger">
                                    <div class="panel-heading panel-1">
                                        <h3 class="panel-title"><center>Informacion del Centro <i class="fa fa-university" aria-hidden="true"></i> </center></h3>
                                    </div>
                                    <div class="panel-body  panel-body-1">
                                        <table class="table table-striped table-hover table-bordered table-responsive">
                                            <tbody  style="font-size: 12px;cursor: pointer;">               
                                               <tr><td>Codigo: <span id="codigo"></span></td></tr>
                                               <tr><td>Direccion: <span id="direccion"></span></td></tr>
                                               <tr><td>Mesas: <span id="mesas"></span></td></tr>
                                               <tr id="_electores"><td>Electores: <span id="electores"></span></td></tr></a>
                                               <tr id="_militante"><td>Militantes: <span id="militante"></span></td></tr>
                                               <tr id="_firmantes"><td>Firmantes: <span id="firmantes"></span></td></tr>
                                               <tr id="_carnet"><td>Carnet: <span id="carnet"></span></td></tr>
                                            </tbody>
                                        </table>   
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-2 hidden-xs "></div>
                            <div class="col-md-5 col-xs-10 col-sm-10 col-lg-5">
                                <div class="panel panel-danger">
                                    <div class="panel-heading panel-1">
                                        <h3 class="panel-title"><center>Informacion de las Mesas</center></h3>
                                    </div>
                                    <div class="panel-body  panel-body-1">
                                         <table class="table table-striped table-hover table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    
                                                    <th style="text-align: center;">Mesa</th>
                                                    <th style="text-align: center;">Electores</th>
                                                    <th style="text-align: center;">PSUV</th>
                                                    <th style="text-align: center;">Firmantes</th>
                                                    <th style="text-align: center;">Carnet</th>
                                                 
                                                </tr>
                                            </thead>

                                            <tbody id="mesas_electorales" style="font-size: 12px;cursor: pointer;">               
                                              
                                            </tbody>
                                        </table>    
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">    
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <center>
                                            <h3 class="panel-title">Tendecia del Comportamento Electoral del Centro  <i class="fa fa-bar-chart" aria-hidden="true"></i></h3>
                                        </center>
                                    </div>
                                    <div class="panel-body"  > 
                                        <div id="grafica_centro"></div>
                                        <!-- Button trigger modal -->
                                    </div>
                                </div>   
                            </div>
                        </div> 
                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">    
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <center>
                                            <h3 class="panel-title"> Tendecia del Comportamento Electoral del Centro  <i class="fa fa-table" aria-hidden="true"></i></h3>
                                        </center>
                                    </div>
                                    <div class="panel-body"  > 
                                         <table class="table table-striped table-hover table-bordered table-responsive" id="task-table">
                                            <thead>
                                                <tr>
                                                    <th><span id="municipio">Mesa</span></th>
                                                    <th colspan="2" style="text-align: center;">REG. 2008</th>
                                                    <th colspan="2" style="text-align: center;">PAR. 2010</th>
                                                    <th colspan="2" style="text-align: center;">PRE. 2012</th>
                                                    <th colspan="2" style="text-align: center;">REG. 2012</th>
                                                    <th colspan="2" style="text-align: center;">PRE. 2013</th>
                                                    <th colspan="2" style="text-align: center;">PAR. 2015</th>
                                                    <th colspan="2" style="text-align: center;">REG. 2017</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listado_mesas" style="font-size: 11px;">
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>   
                            </div>
                        </div> 
                    </div>  
                </div>
                <div class="modal-footer">
                    <center> 
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 37px;">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document"  style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">         
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <?php echo e(Form::bsSelect('elecciones_1',$controller->elecciones(), '', [
                                'label' => 'Elecciones:',
                                'required' => 'required'
                            ])); ?> 
                            <?php echo e(Form::bsSelect('elecciones_2',$controller->elecciones(), '', [
                                'label' => 'Elecciones:',
                                'required' => 'required'
                            ])); ?>


                            <button type="button" class="btn btn-primary" style="height: 37px; margin-top: 2%;" id="comparar2">Comparar</button>

                        </div>
                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">    
                               <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <div class="pull-left">
                                            <h3 class="panel-title">Resultados de Elecciones </h3>
                                        </div> 
                                    </div>
                                    <div class="panel-body" id="buscar"  style="display: none;">
                                        <input type="text" class="form-control" id="task-table-filter" data-action="filter" data-filters="#task-table" placeholder="Filtrar"/>
                                    </div>
                                    <div class="panel-body" style="overflow: scroll;">
                                        <table class="table table-striped table-hover table-bordered table-responsive" id="task-table">
                                            <thead>
                                                <tr>
                                                    <th><span id="municipio">MUNICIPIOS</span></th>
                                                    <th colspan="2" style="text-align: center;"><span id="txt_elecciones1"> Elecciones </span></th>
                                                    <th colspan="2" style="text-align: center;"><span id="txt_elecciones2"> Elecciones </th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                    <th>Resul</th>
                                                    <th>D.V</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="listado_municipios_2" style="font-size: 11px;">
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>  
                </div>
                <div class="modal-footer">
                    <center> 
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 37px;">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
    <script type="text/javascript">
        (function(){
            'use strict';
            var $ = jQuery;
            $.fn.extend({
                filterTable: function(){
                    return this.each(function(){
                        $(this).on('keyup', function(e){
                            $('.filterTable_no_results').remove();
                            var $this = $(this), 
                                search = $this.val().toLowerCase(), 
                                target = $this.attr('data-filters'), 
                                $target = $(target), 
                                $rows = $target.find('tbody tr');
                                
                            if(search == '') {
                                $rows.show(); 
                            } else {
                                $rows.each(function(){
                                    var $this = $(this);
                                    $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                                })
                                if($target.find('tbody tr:visible').size() === 0) {
                                    var col_count = $target.find('tr').first().find('td').size();
                                    var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No Hay Resultado</td></tr>')
                                    $target.find('tbody').append(no_results);
                                }
                            }
                        });
                    });
                }
            });
            $('[data-action="filter"]').filterTable();
        })(jQuery);
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('consultapsuv::layouts.plantilla', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>