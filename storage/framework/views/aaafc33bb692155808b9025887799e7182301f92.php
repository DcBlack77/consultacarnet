<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $__env->make('consultapsuv::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
    </head>
    <body style="font-family: arial !important;">
        
        <div class="container-top"></div>
            <?php echo $__env->yieldContent('content'); ?>
        <div class="container-botton"></div>
    
        <?php echo $__env->make('consultapsuv::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
    </body>
</html>
