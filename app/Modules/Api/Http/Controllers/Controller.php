<?php

namespace App\Modules\Api\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'api';

    public $autenticar = false;


	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Api/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Api/Assets/css',
	];

	public $libreriasIniciales = [

	];

}
