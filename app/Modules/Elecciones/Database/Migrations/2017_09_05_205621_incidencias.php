<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Incidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('estatus_incidencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 100);
           
            $table->timestamps();
            $table->softDeletes();
        });
        /*Comunidad*/
         Schema::create('comunidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parroquias_id')->unsigned();
            $table->string('slug', 250);
            $table->string('nombre', 100);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parroquias_id')->references('id')->on('parroquias')->onDelete('cascade')->onUpdate('cascade');
        });

        /*tabla del lider de calle */

        Schema::create('fuente_informacion', function(Blueprint $table){
            $table->increments('id');
            $table->integer('personas_id')->unsigned();
            $table->integer('municipios_id')->unsigned();
            $table->integer('parroquias_id')->unsigned();
            $table->integer('comunidades_id')->unsigned();
            $table->string('nombre_calle', 200)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('personas_id')->references('id')->on('personas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('municipios_id') ->references('id')->on('municipios')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parroquias_id')->references('id')->on('parroquias')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comunidades_id')->references('id')->on('comunidades')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('usuario_fuente_informacion', function(Blueprint $table){
            $table->increments('id');
            $table->integer('app_usuario_id')->unsigned();
            $table->integer('fuente_informacion_id')->unsigned();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('app_usuario_id')->references('id')->on('app_usuario')->onDelete('cascade')->onUpdate('cascade');;
            $table->foreign('fuente_informacion_id')->references('id')->on('fuente_informacion')->onDelete('cascade')->onUpdate('cascade');;
        });

        Schema::create('incidencia', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->integer('persona_contacto_id')->unsigned();
            $table->integer('app_usuario_id')->unsigned();
            $table->integer('municipios_id')->unsigned();
            $table->integer('parroquias_id')->unsigned();
            $table->integer('estatus_incidencia_id')->unsigned();
            $table->string('zona', 250)->nullable();
            $table->text('descripcion')->nullable();
            $table->text('accion_tomada')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('persona_contacto_id') ->references('id')->on('personas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('app_usuario_id') ->references('id')->on('app_usuario')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('municipios_id') ->references('id')->on('municipios')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parroquias_id')->references('id')->on('parroquias')->onDelete('cascade')->onUpdate('cascade');
             $table->foreign('estatus_incidencia_id')->references('id')->on('estatus_incidencia')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencia');
        Schema::dropIfExists('usuario_fuente_informacion');
        Schema::dropIfExists('fuente_informacion');
        Schema::dropIfExists('comunidades');
        Schema::dropIfExists('estatus_incidencia');
    }
}
