var aplicacion, $form, tabla;
$(function() {
	/*
	aplicacion = new app('formulario', {
		'limpiar' : function(){
		}
	});

	$form = aplicacion.form;*/

	/*eventos del formualrio de carga*/
	$('#carga').clearForm();

	$('#subir_btn').on('click', function(e){
		e.preventDefault();
		$("#subir:hidden").trigger('click');
	});

	$("#tipo_carga").on('change', function(){
		if ($(this).val() != 'rep') {
			return;
		}
		
		var l = Ladda.create($("#subir_btn").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'archivo',
			type : 'POST',
			success: function(r){
				aviso(r);
				$('#carga').clearForm();
			},
			complete : function(){
				l.stop();
				$("#carga").clearForm();
			}
		}; 
		
		$('#carga').ajaxSubmit(options2); 
	});

	$("#subir").on('change', function(){
		var l = Ladda.create($("#subir_btn").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'archivo',
			type : 'POST',
			success: function(r){
				aviso(r);
				$('#carga').clearForm();
			},
			complete : function(){
				l.stop();
				$("#carga").clearForm();
			}
		}; 
		
		$('#carga').ajaxSubmit(options2); 
	});
});