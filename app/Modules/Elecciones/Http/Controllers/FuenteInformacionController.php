<?php

namespace App\Modules\Elecciones\Http\Controllers;

//Controlador Padre
use App\Modules\Elecciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Elecciones\Http\Requests\FuenteInformacionRequest;

//Modelos
use App\Modules\Elecciones\Models\FuenteInformacion;

class FuenteInformacionController extends Controller
{
    protected $titulo = 'Fuente Informacion';

    public $js = [
        'FuenteInformacion'
    ];
    
    public $css = [
        'FuenteInformacion'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('elecciones::FuenteInformacion', [
            'FuenteInformacion' => new FuenteInformacion()
        ]);
    }

    public function nuevo()
    {
        $FuenteInformacion = new FuenteInformacion();
        return $this->view('elecciones::FuenteInformacion', [
            'layouts' => 'base::layouts.popup',
            'FuenteInformacion' => $FuenteInformacion
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $FuenteInformacion = FuenteInformacion::find($id);
        return $this->view('elecciones::FuenteInformacion', [
            'layouts' => 'base::layouts.popup',
            'FuenteInformacion' => $FuenteInformacion
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $FuenteInformacion = FuenteInformacion::withTrashed()->find($id);
        } else {
            $FuenteInformacion = FuenteInformacion::find($id);
        }

        if ($FuenteInformacion) {
            return array_merge($FuenteInformacion->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(FuenteInformacionRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $FuenteInformacion = $id == 0 ? new FuenteInformacion() : FuenteInformacion::find($id);

            $FuenteInformacion->fill($request->all());
            $FuenteInformacion->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $FuenteInformacion->id,
            'texto' => $FuenteInformacion->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            FuenteInformacion::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            FuenteInformacion::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            FuenteInformacion::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = FuenteInformacion::select([
            'id', 'personas_id', 'municipios_id', 'parroquias_id', 'comunidades_id', 'nombre_calle', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}