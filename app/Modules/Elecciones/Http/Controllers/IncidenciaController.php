<?php

namespace App\Modules\Elecciones\Http\Controllers;

//Controlador Padre
use App\Modules\Elecciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Elecciones\Http\Requests\IncidenciaRequest;

//Modelos
use App\Modules\Elecciones\Models\Incidencia;
use App\Modules\Elecciones\Models\FuenteInformacion;
use App\Modules\Elecciones\Models\UsuarioFuenteInformacion;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Base\Models\Parroquia;
use App\Modules\Base\Models\TipoTelefono;

class IncidenciaController extends Controller
{
    protected $titulo = 'Incidencia';

    public $js = [
        'Incidencia'
    ];
    
    public $css = [
        'Incidencia'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'template',
    ];

    public function index()
    {
        return $this->view('elecciones::Incidencia', [
            'Incidencia' => new Incidencia()
        ]);
    }

    public function nuevo()
    {
        $Incidencia = new Incidencia();
        return $this->view('elecciones::Incidencia', [
            'layouts' => 'base::layouts.popup',
            'Incidencia' => $Incidencia
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Incidencia = Incidencia::find($id);
        return $this->view('elecciones::Incidencia', [
            'layouts' => 'base::layouts.popup',
            'Incidencia' => $Incidencia
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Incidencia = Incidencia::withTrashed()->find($id);
        } else {
            $Incidencia = Incidencia::find($id);
        }


        if ($Incidencia) {

            $parroquia = Parroquia::where('municipios_id', $Incidencia->municipios_id)
                ->pluck('nombre','id')
                ->put('_', $Incidencia->parroquias_id);

           // dd($parroquia);

            $persona = Personas::where('id', $Incidencia->persona_contacto_id)->first();

            $Incidencia->persona_contacto_id = $persona->id;
            $Incidencia->cedula = $persona->dni;
            $Incidencia->nombre_persona = $persona->nombres;
            $Incidencia->telefono = $persona->personastelefono->toArray();
            $Incidencia->parroquias_id = $parroquia;

            return array_merge($Incidencia->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar'),

            ]);
        }

        return trans('controller.nobuscar');
    }

    public function data($request){
        $datos = $request->all();

        $datos['app_usuario_id'] = \Auth::user()->id;
        
        return $datos;
    }

    public function telefono_guardar($request, $persona_id){
        DB::beginTransaction();
        try { 

            foreach ($request->tipo_telefono_id as $key => $value) {
                //dd($request->numero[$key]);
                $telefono = PersonasTelefono::firstOrNew([
                    'id'  => $request->id_telefono[$key]
                ])->fill([
                    'personas_id'       => $persona_id,
                    'principal'         => 'f',
                    'tipo_telefono_id'  => $request->tipo_telefono_id[$key],
                    'numero'            => $request->numero[$key]
                ])->save();
                /*
                 if($request->id_telefono[$key]){
                    PersonasTelefono::create($datos);
                }else{
                    PersonasTelefono::find($request['id_telefonos'][$_id])->update($datos);
                } */
            }

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }

    public function guardar(IncidenciaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{

            $data =  $this->data($request);

            $Incidencia = $id == 0 ? new Incidencia() : Incidencia::find($id);

            $Incidencia->fill($data);
            $Incidencia->save();

            $this->telefono_guardar($request, $request->persona_contacto_id);

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Incidencia->id,
            'texto' => $Incidencia->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Incidencia::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Incidencia::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Incidencia::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Incidencia::select([
            'id', 'fecha', 'app_usuario_id', 'municipios_id', 'parroquias_id', 'estatus_incidencia_id', 'zona', 'descripcion', 'accion_tomada', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function buscarPersona(Request $request, $id = 0){
        $persona =  Personas::where('dni', $id)->first();
        $usuario = \Auth::user()->id;

        $salida = ['s'=> 'n', 'msj' => 'No se encontró ninguna persona'];
        
        if($persona){
            $FuenteInformacion = FuenteInformacion::where('personas_id', $persona->id)->first();
            //dd($FuenteInformacion);
            $asignado_otro = UsuarioFuenteInformacion::where('fuente_informacion_id', $FuenteInformacion->id)->first();

            $msj_asignado= [ 's'=> 'n', 'msj' => $persona->nombres . ' se encuentra asignada a otro usuario'];

            if(!$asignado_otro){

                $asignado = UsuarioFuenteInformacion::create([
                    "app_usuario_id"=> $usuario,
                    "fuente_informacion_id"=> $FuenteInformacion->id
                ]);

                $msj_asignado = [ 's'=> 's', 'msj' => 'Se ha asignado la persona exitosamente a su usuario'];
            
            }else{
                if($asignado_otro->app_usuario_id == $usuario){
                    $msj_asignado= [ 's'=> 's', 'msj' => $persona->nombres . ' ya se encuentra asignada a usted'];
                }
            }

            /*estableciendo la ubicación*/
            
            $parroquia_id = Parroquia::where('municipios_id', $FuenteInformacion->municipios_id)
                ->pluck('nombre','id')
                ->put('_', $FuenteInformacion->parroquias_id);

            $salida= [
                's' => 's',
                'msj' => 'Persona Encontrada',
                'persona' => $persona,
                'telefono' => $persona->personastelefono->toArray(),
                'asignado' => $msj_asignado,
                'municipios_id' => $FuenteInformacion->municipios_id,
                'parroquias_id' => $parroquia_id,
                'zona' => $FuenteInformacion->nombre_calle

            ];

        }
        return $salida;
    }

    public function parroquias(Request $request){
        $sql = Parroquia::where('municipios_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'parroquias_id'=> $sql];
        }               
        
        return $salida;
    }

    public function tipoTelefono(){
        return TipoTelefono::pluck('nombre','id');
    }
}