<?php

namespace App\Modules\Elecciones\Http\Controllers;

//Controlador Padre
use App\Modules\Elecciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Excel;

//Request
use App\Modules\Elecciones\Http\Requests\EstatusIncidenciaRequest;

//Modelos
use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Elecciones\Models\Comunidades;
use App\Modules\Elecciones\Models\FuenteInformacion;

use App\Modules\Consultapsuv\Models\Centros;
use App\Modules\Consultapsuv\Models\Rep;

class CargaController extends Controller
{
    protected $titulo = 'Cargar Data';

    public $js = [
        'Carga'
    ];
    
    public $css = [
        'Carga'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'maskedinput',
        'ladda',
    ];

    public function index()
    {
        return $this->view('elecciones::Carga');
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EstatusIncidencia = EstatusIncidencia::withTrashed()->find($id);
        } else {
            $EstatusIncidencia = EstatusIncidencia::find($id);
        }

        if ($EstatusIncidencia) {
            return array_merge($EstatusIncidencia->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(Request $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EstatusIncidencia = $id == 0 ? new EstatusIncidencia() : EstatusIncidencia::find($id);

            $EstatusIncidencia->fill($request->all());
            $EstatusIncidencia->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $EstatusIncidencia->id,
            'texto' => $EstatusIncidencia->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EstatusIncidencia::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EstatusIncidencia::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EstatusIncidencia::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EstatusIncidencia::select([
            'id', 'descripcion', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function archivo(Request $request)
    {
        $tipo_carga = $request->tipo_carga;

        if ($tipo_carga == 'rep') {
            return $this->procesar_rep($request);
        }
        return $this->_archivo($request);
    }

    public function procesar_rep(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        Excel::load('storage/app/rep.xlsx', function($reader) {
            //$reader->setDateFormat('Y-m-d');
            
            foreach ($reader->get() as $linea) {
                //dd($linea);
                $linea->nacionalidad  = strtolower($linea->nacionalidad);
                $linea->dni           = intval($linea->dni);
                $linea->nombre        = ucwords(strtolower($linea->nombre));
                $linea->genero        = strtolower($linea->genero);
                //$linea->nacimiento    = $linea->nacimiento;
                $linea->centro_id     = intval($linea->centro_id);
                $linea->municipio     = ucwords(strtolower($linea->municipio));
                $linea->parroquia     = ucwords(strtolower($linea->parroquia));
                $linea->nombre_centro = ucwords(strtolower($linea->nombre_centro));
                $linea->direccion     = ucfirst(strtolower($linea->direccion));

                $linea->fch           = strtolower($linea->fch)  == 's' ? 1 : 0;
                $linea->fcm           = strtolower($linea->fcm)  == 's' ? 1 : 0;
                $linea->cp            = strtolower($linea->cp)   == 's' ? 1 : 0;
                $linea->psuv          = strtolower($linea->psuv) == 's' ? 1 : 0;

                $linea->movil         = $linea->movil1;
                $linea->telefono      = $linea->telefono1;
                $linea->telf          = $linea->telf;

                $centro = Centros::find($linea->centro_id);

                if ($centro) {
                    $centro->nombre = $linea->nombre_centro;
                    $centro->municipio = $linea->municipio;
                    $centro->parroquia = $linea->parroquia;
                    $centro->direccion = $linea->direccion;
                    $centro->save();
                }
                
                Rep::create([
                    'nacionalidad' => $linea->nacionalidad,
                    'dni'          => $linea->dni,
                    'nombre'       => $linea->nombre,
                    'genero'       => $linea->genero,
                    'nacimiento'   => $linea->nacimiento,
                    'centro_id'    => $linea->centro_id,
                    'municipio'    => $linea->municipio,
                    'parroquia'    => $linea->parroquia,
                    'direccion'    => $linea->direccion,
                    'movil'        => $linea->movil,
                    'telefono'     => $linea->telefono,
                    'tlf'          => $linea->tlf,
                    'fch'          => $linea->fch,
                    'fcm'          => $linea->fcm,
                    'cp'           => $linea->cp,
                    'psuv'         => $linea->psuv,
                    'gob'          => 0,
                    'fd'           => 0,
                ]);
            }

            //DB::commit();
        });

        return "listo :: " . date('d/m/Y H:i:s');
    }

    public function _archivo(Request $request)
    {
        $mimes = [
            'text/csv',
            'application/vnd.ms-excel',
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];

        $ruta = public_path('archivos/');
        $archivo = $request->file('subir');

        $mime = $archivo->getClientMimeType();

        if (!in_array($mime, $mimes)) {
            return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
        }

        do {
            $nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
        } while (is_file($ruta . $nombre_archivo));

    
        $archivo->move($ruta, $nombre_archivo);
        
        chmod($ruta . $nombre_archivo, 0777);

        $xls = $ruta . $nombre_archivo;

        $datos = Excel::selectSheetsByIndex(0)->load($xls)->get();

        $municipios = [];
        foreach (Municipio::pluck('nombre', 'id') as $municipios_id => $municipio) {
            $slug = str_slug($municipio, '-');
            $municipios[$slug] = intval($municipios_id);
        }

        $parroquias = [];
        foreach (Parroquia::pluck('nombre', 'id') as $parroquias_id => $parroquia) {
            $slug = str_slug($parroquia, '-');
            $parroquias[$slug] = intval($parroquias_id);
        }

        $parroquias_c = collect(Parroquia::all());

        try {
            set_time_limit(0);
            DB::beginTransaction();

            foreach($datos as $dato){
                //dd($dato);
                $municipio_slug = str_replace('nn', 'n', str_slug($dato->municipio, '-'));
                $parroquia_slug = str_replace('nn', 'n', str_slug($dato->parroquia, '-'));
                $comunidad_slug = str_replace('nn', 'n', str_slug($dato->comunidad, '-'));
                /*
                "municipio" => "CARONI"
                "parroquia" => "CACHAMAY"
                "nombre_calle" => "ACARIGUA"
                "nombre_jefe" => "ODAHILDA SALLET GIL HERNANDEZ"
                "cedula" => 6437771.0
                "telefono" => 4169999438.0
                "comunidad" => "CARONI UD-220"
  
            
                $parroquia = Parroquia::updateOrCreate([
                    ''
                    'id' => $parroquias[$parroquia_slug]
                ]);

                $parroquia->nombre = utf8_encode(ucwords(strtolower($dato->parroquia)));
                $parroquia->municipios_id = $municipios[$municipio_slug];
                $parroquia->save();
                */
                 /*if(!isset($municipios[$municipio_slug])){
                    dd('no existe');
                    Municipio::create([
                       "estados_id" => 6,
                       "nombre" => utf8_encode(ucwords(strtolower($dato->municipio)))
                    ]);

                    $municipios = [];
                    foreach (Municipio::pluck('nombre', 'id') as $municipios_id => $municipio) {
                        $slug = str_slug($municipio, '-');
                        $municipios[$slug] = intval($municipios_id);
                    }
                }*/

                if(!isset($parroquias[$parroquia_slug])){
                    dd('no existe');
                   /* Parroquia::create([
                        "nombre" =>  utf8_encode(ucwords(strtolower($dato->parroquia))),
                        "municipios_id" => $municipios[$municipio_slug]
                    ]);
                    $parroquias = [];
                    foreach (Parroquia::pluck('nombre', 'id') as $parroquias_id => $parroquia) {
                        $slug = str_slug($parroquia, '-');
                        $parroquias[$slug] = intval($parroquias_id);
                    }*/
                }
                

                $comunidad = Comunidades::firstOrNew([
                    'slug' => $comunidad_slug,
                    'parroquias_id' => $parroquias[$parroquia_slug],
                ]);

                $comunidad->nombre = utf8_encode(ucwords(strtolower($dato->comunidad)));
                $comunidad->save();

                $buscar_persona = Personas::where('dni', 'like', '%'.intval($dato->cedula).'%' )->first();
                
                if (!$buscar_persona) {
                    $persona =  Personas::create([
                        "tipo_persona_id" => 1,
                        "dni" =>intval($dato->cedula),
                        "nombres" => utf8_encode(ucfirst(strtolower($dato->nombre_jefe))),
                    ]);

                }else{
                    $persona = $buscar_persona;
                }


                //telefonos 
                $telefono = '';
                if(intval($dato->telefono) != 0){

                    $telefono = PersonasTelefono::firstOrNew([
                        "personas_id" => $persona->id,
                        "numero" => strval($dato->telefono),
                    ]);

                    $telefono->tipo_telefono_id = 1;
                    $telefono->principal = 1;
                    $telefono->save();
                }

                //fuente informacion
                $fuente_informacion = FuenteInformacion::firstOrNew([
                    "personas_id" => $persona->id,
                    "municipios_id" => $municipios[$municipio_slug],
                    "parroquias_id" => $parroquias[$parroquia_slug],
                    "comunidades_id" => $comunidad->id,
                ]);

                $fuente_informacion->nombre_calle = utf8_encode(ucfirst(strtolower($dato->nombre_calle)));
                $fuente_informacion->save();
                
                
                $registro = [
                    'persona' => $persona,
                    'telefono' => $telefono,
                    'comunidad' =>$comunidad,
                    'fuente_informacion' => $fuente_informacion,
                ];
            }
            
            DB::commit();
        } catch (Exception $e) {
            dd($e);
            DB::rollback();
        }

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }
}