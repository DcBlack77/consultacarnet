<?php

namespace Modules\Elecciones\Http\Requests;

use App\Http\Requests\Request;

class ComunidadesRequest extends Request {
    protected $reglasArr = [
		'descripcion' => ['required', 'min:3', 'max:100']
	];
}