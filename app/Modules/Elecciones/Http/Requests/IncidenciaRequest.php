<?php

namespace App\Modules\Elecciones\Http\Requests;

use App\Http\Requests\Request;

class IncidenciaRequest extends Request {
    protected $reglasArr = [
		'fecha' => ['required'], 
		'persona_contacto_id' => ['integer', 'required'], 
		'parroquias_id' => ['integer', 'required'], 
		'estatus_incidencia_id' => ['integer', 'required'], 
		'zona' => ['min:3', 'max:250']
	];
}