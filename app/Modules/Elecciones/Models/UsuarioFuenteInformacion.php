<?php

namespace App\Modules\Elecciones\Models;

use App\Modules\Base\Models\Modelo;



class UsuarioFuenteInformacion extends Modelo
{
    protected $table = 'usuario_fuente_informacion';
    protected $fillable = ["app_usuario_id","fuente_informacion_id"];
    protected $campos = [
    'app_usuario_id' => [
        'type' => 'number',
        'label' => 'App Usuario',
        'placeholder' => 'App Usuario del Usuario Fuente Informacion'
    ],
    'fuente_informacion_id' => [
        'type' => 'number',
        'label' => 'Fuente Informacion',
        'placeholder' => 'Fuente Informacion del Usuario Fuente Informacion'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}