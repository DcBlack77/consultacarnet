<?php

namespace App\Modules\Elecciones\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;
use App\Modules\Elecciones\Models\Comunidades;
use App\Modules\Elecciones\Models\EstatusIncidencia;



class Incidencia extends modelo
{
    protected $table = 'incidencia';
    protected $fillable = [
        "fecha",
        "persona_contacto_id",
        "app_usuario_id",
        "municipios_id",
        "parroquias_id",
        "estatus_incidencia_id",
        "zona",
        "descripcion",
        "accion_tomada"
    ];
    protected $campos = [
        'fecha' => [
            'type' => 'text',
            'label' => 'Fecha',
            'placeholder' => 'Fecha del Incidencia'
        ],
        'app_usuario_id' => [
            'type' => 'number',
            'label' => 'App Usuario',
            'placeholder' => 'App Usuario del Incidencia'
        ],
        'municipios_id' => [
            'type' => 'select',
            'label' => 'Municipios',
            'placeholder' => 'Municipios',
            'url' => 'municipios'
        ],
        'parroquias_id' => [
            'type' => 'select',
            'label' => 'Parroquias',
            'placeholder' => 'Parroquias del Incidencia',
            'url' => 'parroquias'
        ],
        'estatus_incidencia_id' => [
            'type' => 'select',
            'label' => 'Estatus Incidencia',
            'placeholder' => 'Estatus Incidencia',
            'url' => 'estatus_incidencia'
        ],
        'zona' => [
            'type' => 'text',
            'label' => 'Zona',
            'placeholder' => 'Zona del Incidencia'
        ],
        'descripcion' => [
            'type' => 'textarea',
            'label' => 'Descripcion',
            'placeholder' => 'Descripcion del Incidencia',
            'cont_class' => 'col-xs-12'
        ],
        'accion_tomada' => [
            'type' => 'textarea',
            'label' => 'Accion Tomada',
            'placeholder' => 'Accion Tomada del Incidencia',
            'cont_class' => 'col-xs-12'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['municipios_id']['options'] = Municipio::where('estados_id', 6)->pluck('nombre', 'id');
        $this->campos['estatus_incidencia_id']['options'] = EstatusIncidencia::pluck('descripcion', 'id');
    }

    
}