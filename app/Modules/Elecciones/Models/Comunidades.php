<?php

namespace App\Modules\Elecciones\Models;

use App\Modules\Base\Models\Modelo;



class Comunidades extends modelo
{
    protected $table = 'comunidades';
    protected $fillable = ["parroquias_id","slug","nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Descripcion',
        'placeholder' => 'Descripcion del Comunidades'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}