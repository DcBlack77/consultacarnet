@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Estatus Incidencia']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar EstatusIncidencia.',
        'columnas' => [
            'Descripcion' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $EstatusIncidencia->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection