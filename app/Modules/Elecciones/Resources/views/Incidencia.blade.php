@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
        
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Incidencia.',
        'columnas' => [
            'Fecha' => '12.5',
		'App Usuario' => '12.5',
		'Municipios' => '12.5',
		'Parroquias' => '12.5',
		'Estatus Incidencia' => '12.5',
		'Zona' => '12.5',
		'Descripcion' => '12.5',
		'Accion Tomada' => '12.5'
        ]
    ])
@endsection

@section('content')
    <div class="container">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            <input type="hidden" name="app_usuario_id" id="app_usuario_id"/>
            <!--fuente de informacion-->
            <div class="col-xs-12 subtitulo">
                <h4>Persona de Contacto</h4>
            </div>
            
            <input type="hidden" name="persona_contacto_id" id="persona_contacto_id" />

            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <label for="Cedula">Cédula:</label>
                <input placeholder="Cédula de la persona" id="cedula" class="form-control" name="cedula" type="text">
            </div>
            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <label for="Cedula">Nombre:</label>
                <div class="form-control" id="nombre_persona"></div>
            </div>

            <!--telefonos adicionales-->
            <div class="col-xs-12 subtitulo">
                <h4>Teléfonos adicionales</h4>
            </div>
            
            <div class="col-xs-12">
                <table id="tablaTelefonos" class="table table-bordered table-striped table-condensed flip-content">
                    <thead style="background: #fff;">
                        <tr>
                            <th>Tipo Teléfono</th>
                            <th>Número</th>
                            
                            <th style="width: 90px;">
                                <div class="btn-group btn-group-sm btn-group-solid">
                                    <button id="remover" type="button" class="btn red"><i class="fa fa-remove"></i></button>
                                    <button id="agregar" type="button" class="btn blue"><i class="fa fa-plus"></i></button>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <!--Información recaudada-->
            <div class="col-xs-12 subtitulo">
                <h4>Información recaudada</h4>
            </div>    
           
            {!! $Incidencia->generate(['fecha','municipios_id','parroquias_id','zona','estatus_incidencia_id','descripcion','accion_tomada']) !!}

        {!! Form::close() !!}
    </div>
@endsection
@push('css')
<style type="text/css">
    .subtitulo h4{
        color: #8d0909;
    }
</style>
@endpush
@push('js')
    <script type="text/x-tmpl" id="tmpl-telefonos">
        {% for (var i = 0; i < o.telefono.length; i++) { %}
            <tr>
                <input type="hidden" name="id_telefono[]" value="{%=o.telefono[i].id%}"/>
                
                <td><input name="numero[]" class="form-control" placeholder="Número de telefono" type="text" value="{%=o.telefono[i].numero%}"></td>
                <td>
                    <select class="form-control" name="tipo_telefono_id[]">
                        <option value="">Seleccione ..</option>
                        @foreach($controller->tipoTelefono() as $id => $telefono)
                            <option value="{{ $id }}" {% if (o.telefono[i].tipo_telefono_id == {{ $id }}) { %} selected="selected" {% } %}>
                                {{ $telefono }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <button type="button" class="btn red"><i class="fa fa-remove"></i></button>
                </td>
            </tr>
            
        {% } %}
    </script>
@endpush