@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
        
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar FuenteInformacion.',
        'columnas' => [
            'Personas' => '20',
		'Municipios' => '20',
		'Parroquias' => '20',
		'Comunidades' => '20',
		'Nombre Calle' => '20'
        ]
    ])
@endsection

@section('content')
    <div class="container">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $FuenteInformacion->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection