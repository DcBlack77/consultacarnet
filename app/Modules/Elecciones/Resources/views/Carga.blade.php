@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content')
    <div class="container">
        <div class="col-xs-12 subtitulo">
            <h4>Cargar Archivo <small>Excel (.csv, .xls, .xlsx, .ods)</small></h4>
            
            {!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
                <input id="subir" name="subir" type="file"/>
                {{ Form::bsSelect('tipo_carga', [
                    'lideres' => 'Lideres de Calle',
                    'rep' => 'REP',
                ], 'lideres',
                [
                    'label' => 'Tipo de carga',
                    'required' => 'required'
                ]) }}
                <button id="subir_btn" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="icon-arrow-right"></i> Carga archivo Excel
                    </span>
                </button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@push('css')
<style type="text/css">
    #subir {
        display: none;
    }
</style>
@endpush
