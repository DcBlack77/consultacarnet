<!DOCTYPE html>
<html lang="en">
    <head>
        @include('consultapsuv::partials.head')
        
    </head>
    <body style="font-family: arial !important;">
        
        <div class="container-top"></div>
            @yield('content')
        <div class="container-botton"></div>
    
        @include('consultapsuv::partials.footer')  
    </body>
</html>
