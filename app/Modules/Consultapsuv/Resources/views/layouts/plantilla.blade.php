<!DOCTYPE html>
<html lang="en">
    <head>
        @include('consultapsuv::partials.head')
        <!--Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lalezar&amp;subset=arabic,latin-ext,vietnamese" rel="stylesheet">
      {{--    <link href="{{url('public/fonts/pt-sams.ttf')}}" rel="stylesheet">
        <link href="{{url('public/fonts/lalezar.ttf')}}" rel="stylesheet">  --}}
    </head>
    <body>
        @include('consultapsuv::partials.page-header') 
        
        <div class="container-top">
            @yield('content-top')
        </div>
            @yield('content')
        <div class="container-botton"></div>
        @include('consultapsuv::partials.page-footer') 
      
        @include('consultapsuv::partials.footer')  
    </body>
</html>
