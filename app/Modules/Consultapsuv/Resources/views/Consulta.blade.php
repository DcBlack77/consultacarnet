@extends('consultapsuv::layouts.popup')
@section('content')


<div class="container">
    <div class="row">

        <div class="col-xs-12  col-md-4">
            <div class="col-xs-12  col-md-12">
                <div class="widget-author  boxed  push-down-30">
                    <div class="widget-author__image-container">
                        <div class="widget-author__avatar--blurred">

                            @if(file_exists("public/img/fotos/".$ci. ".jpg"))
                            <img src="{{url('public/img/fotos/'. $ci. '.jpg')}}" alt="Avatar image" width="90" height="90">
                            @else
                                <img src="{{url('public/img/usuarios/user.png')}}" alt="Avatar image" width="90" height="90">
                            @endif

                        </div>
                        @if(file_exists("public/img/fotos/".$ci. ".jpg"))
                            <img class="widget-author__avatar" src="{{url('public/img/fotos/'. $ci. '.jpg')}}" alt="Avatar image" width="90" height="90">
                        @else
                            <img class="widget-author__avatar" src="{{url('public/img/usuarios/user.png')}}" alt="Avatar image" width="90" height="90">
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-10  col-xs-offset-1">
                            <br><br><br>
                        </div>
                        <div class="xxx">
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="1">
                                    <div class="well2" data-tipo="1">
                                        <center><h3>Informacion Personal</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td class="b"> <center><b>Nombres y Apellidos</b></center></td>
                                                    <td>{{$informacion_personal->nombre}}</td>
                                                </tr>

                                                <tr>
                                                    <td><center><b>Cédula </b></center></td>
                                                    <td>{{$informacion_personal->dni}} </td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Fecha de Nacimiento </b></center></td>
                                                    <td> {{$informacion_personal->nacimiento}}</td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Edad </b></center></td>
                                                    <td> {{$informacion_personal->edad}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="2">
                                    <div class="well2" data-tipo="2">
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left;">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Municipio:</b></center></td>
                                                    <td>{{$rep_psuv2010->municipio_nombre}}</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Parroquia:</b></center></td>
                                                    <td>{{$rep_psuv2010->parroquia_nombre}}</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Direccion:</b></center></td>
                                                    <td>{{$rep_psuv2010->direccion}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="3">
                                    <div class="well2" data-tipo="3">
                                        <center><h3>Contacto</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Teléfono:</b></td>
                                                    <td>{{$informacion_personal->telefono}}</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Correo:</b></td>
                                                    <td>{{$psa_bolivar->correo_electronico}}</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Twitter:</b></td>
                                                    <td>{{$psa_bolivar->twitter}}</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Instagram:</b></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="5">
                                    <div class="well2" data-tipo="4">
                                        <center><h3>Carnet de la patria</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tr>
                                                <td><center><b>Carnet de la patria</b></center></td>
                                                <td>
                                                    @if($informacion_personal->cp == 1)
                                                    SI
                                                    @else
                                                    No
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Estatus:</b></center></td>
                                                <td>{{$patria_carnet->estatus}}</td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Sub Ambito:</b></center></td>
                                                <td> {{$patria_carnet->sub_ambito}}</td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Parentesco:</b> </center></td>
                                                <td>{{$patria_carnet->parentesco}}</td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Ambito Especifico:</b></center></td>
                                                <td>{{$patria_carnet->ambito_especifico}}</td>
                                            </tr>
                                            <tr>
                                                <td> <center><b>Sub Ambito:</b></center> </td>
                                                <td> {{$patria_carnet->sub_ambito}}</td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12  col-md-8">
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"><center><h3 class="panel-title">Datos Electorales</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos Electorales-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Grafico Tendencia Mesa Donde Vota
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                                <div id="grafica_centro" ></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos Electorales-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Datos Electorales Actualizados
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>

                                                                    @if($varios_votos->v7oct == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Voto Elecciones Chavez 2012</b> <br />

                                                                    @if($varios_votos->v14abr == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Voto Elecciones Maduro 2013</b> <br />

                                                                    @if($varios_votos->v28jul == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Voto Primaria PSUV (AN):</b>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    @if($varios_votos->primud == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Voto Primaria de la MUD</b> <br />

                                                                    @if($varios_votos->v6d == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Voto Asamblea Nacional</b> <br />

                                                                    @if($varios_votos->ubch == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Miembro UBCH</b>

                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">

                                                                    @if($varios_votos->firmo == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Firmo Referendum de Chavez</b> <br />

                                                                    @if($varios_votos->firma_maduro == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif

                                                                    <b>Firmo Referendum de Maduro</b> <br>  <br>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fin de cheka -->



                                <!-- inic -->
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos del centro Electoral-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Datos Del Centro Electoral
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    <b>Codigo del centro Electrol:</b> {{$info_centro->id}}  <br>
                                                                    <b>Nombre del Centro Electoral:</b> {{$info_centro->nombre}} <br>
                                                                    <b>N° Mesa donde Vota:</b>  {{$informacion_personal->mesa}}<br>
                                                                    <b>Tecnologia de la Mesa:</b><br>
                                                                    <b>Elctores:</b>{{$info_centro->electores}} <br>
                                                                    <b>Militantes Psuv / REP:</b> {{$info_centro->militantes}}<br> <br>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    <b>Firmaron:</b><br>
                                                                    <b>Registrado 2011 como</b>  <br>
                                                                    <b>Direccion:</b> {{$info_centro->direccion}} <br>
                                                                    <b>Parroquia:</b> {{$info_centro->parroquia}}<br>
                                                                    <b>Municipio:</b> {{$info_centro->municipio}}<br> <br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fn -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"><center><h3 class="panel-title">Datos Politicos</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <div class="col-md-2 col-xs-12 col-sm-12 titulo_demostracion">PSUV</div>
                                                        <div class="col-md-12 col-xs-12 col-sm-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    @if($informacion_personal->psuv == 1)
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Inscrito PSUV</b>  </br>

                                                                    @if($inscritos->inscrito_2007 == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Inscrito 2007</b> </br>

                                                                    @if($inscritos->inscrito_2009 == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Inscrito 2009</b> </br>

                                                                    @if($inscritos->actualizado_2009 == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Actualizado 2009</b> </br>

                                                                    @if($inscritos->inscrito_2010 == 'SI')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Inscrito 2010</b> </br>

                                                                    <hr>
                                                                    @if($hogares_patria->direccion_hp != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Pertenece a Hogares Patria 2013</b> </br>

                                                                    @if($ccc2012->cargo_cce != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Participo como Equipo C.C.E 2013</b>  </br>

                                                                    @if($ccc_municipales_2013->cargo_ccm != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Participo como Equipo C.C.M 2013</b> <br>

                                                                    @if($ccc_parroquiales_2013->cargo_ccp != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Participo como Equipo C.C.P 2013</b> <br>

                                                                    @if($anillo_2010->anillo != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                    @endif
                                                                    <b>Anillo N&uacutemero: {{$anillo_2010->anillo}}</b>  <br>


                                                                    @if($ubc_resp1x10->codigo_centro_ubch_1x10 != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                        <b>Responsable 1x10 Centro:</b> <a id="modal_centro" style="cursor: pointer;">{{$ubc_resp1x10->codigo_centro_ubch_1x10}}</a><br>
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                        <b>Responsable 1x10 Centro: </b><br>
                                                                    @endif

                                                                    <hr>
                                                                    @if($pat_territoriales_tmunicipios->ced_patrulla != '')
                                                                        <img src='{{url("public/img/check.png")}}' style='width: 24px; height: 24px;' />
                                                                        <b>Patrulla Territorial: </b> <a id="modal_territoriales_m" style="cursor: pointer;">{{$pat_territoriales_tmunicipios->codigo_pat_territorial}}</a><br>
                                                                        @if($pat_territoriales_tmunicipios->ced_resp == $pat_territoriales_tmunicipios->ced_patrulla)
                                                                            <b>Responsable: </b> SI <br/>
                                                                        @else
                                                                            <b>Responsable: </b> NO <br/>
                                                                        @endif
                                                                    @else
                                                                        <img src='{{url("public/img/check2.png")}}' style='width: 24px; height: 24px;' />
                                                                        <b>Patrulla Territorial: </b> <br>
                                                                        <b>Responsable: </b> NO <br/>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- fin de cheka -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3"><center><h3 class="panel-title">Datos Sociales</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                        </div>
                                    </div>
                                    <!-- fin de cheka -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4"><center><h3 class="panel-title">Ayudas Sociales</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                        </div>
                                    </div>
                                    <!-- fin de cheka -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- fin de row -->

    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document"  style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        <span class="nombre_centro"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-danger">
                                    <div class="panel-heading panel-1">
                                        <h3 class="panel-title"><center>Informacion del Centro <i class="fa fa-university" aria-hidden="true"></i> </center></h3>
                                    </div>
                                    <div class="panel-body  panel-body-1">
                                        <table class="table table-striped table-hover table-bordered table-responsive">
                                            <tbody  style="font-size: 12px;cursor: pointer;">
                                               <tr>
                                                    <td style="width: 50%; " >Municipio: {{$info_centro->municipio}}</td>
                                                    <td style="width: 50%; ">Parroquia: {{$info_centro->parroquia}}</td>
                                               </tr>
                                               <tr>
                                                    <td style="width: 50%; ">Codigo: {{$info_centro->id}}</td>
                                                    <td style="width: 50%; ">Direccion:{{$info_centro->direccion}} </td>
                                               </tr>
                                               <tr>
                                                    <td style="width: 50%; ">Mesas: {{$info_centro->mesas}}</td>
                                                    <td style="width: 50%; ">Electores:{{$info_centro->electores}} </td>
                                               </tr>

                                               <tr>
                                                    <td style="width: 50%; ">Militantes:{{$info_centro->militantes}} </td>
                                                    <td style="width: 50%; ">Tecnologia: </td>
                                               </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <center>
                                            <h3 class="panel-title"> <i class="fa fa-table" aria-hidden="true"></i></h3>
                                        </center>
                                    </div>
                                    <div class="panel-body"  >
                                        <center>
                                            <table id="tabla" class="table table-striped table-hover table-bordered tables-text">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%; text-align: center;">Cedula</th>
                                                        <th style="width: 20%; text-align: center;">Nombres</th>
                                                        <th style="width: 20%; text-align: center;">Apellidos</th>
                                                        <th style="width: 10%; text-align: center;">Telefono</th>
                                                        <th style="width: 10%; text-align: center;">Codigo Centro</th>
                                                        <th style="width: 60%; text-align: center;">Nombre Centro</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 37px;">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalLong-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document"  style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">
                       <i class="fa fa-table" aria-hidden="true"></i>
                        Patrulla Territorial: {{$pat_territoriales_tmunicipios->codigo_pat_territorial}}
                        <span class="nombre_centro"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <center>
                                            <h3 class="panel-title"> <i class="fa fa-table" aria-hidden="true"></i></h3>
                                        </center>
                                    </div>
                                    <div class="panel-body"  >
                                        <center>
                                            <table id="tabla2" class="table table-striped table-hover table-bordered tables-text">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20%; text-align: center;">Cedula</th>
                                                        <th style="width: 40%; text-align: center;">Nombres y Apellidos</th>

                                                        <th style="width: 20%; text-align: center;">Municipio</th>
                                                        <th style="width: 20%; text-align: center;">Parroquia</th>

                                                    </tr>
                                                </thead>
                                            </table>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 37px;">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalLong-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document"  style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">
                       <i class="fa fa-table" aria-hidden="true"></i>
                        Patrulla Sectorial: {{$pat_sectoriales_municipios->codigo_pat_sectorial}}
                        <span class="nombre_centro"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                           <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="background: whitesmoke !important;height: 41px;color: black;" >
                                        <center>
                                            <h3 class="panel-title"> <i class="fa fa-table" aria-hidden="true"></i></h3>
                                        </center>
                                    </div>
                                    <div class="panel-body"  >
                                        <center>
                                            <table id="tabla3" class="table table-striped table-hover table-bordered tables-text">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 11.11%; text-align: center;">Cedula</th>
                                                        <th style="width: 11.11%; text-align: center;">Nombres</th>
                                                        <th style="width: 11.11%; text-align: center;">Apellidos</th>
                                                        <th style="width: 11.11%; text-align: center;">telefono</th>
                                                        <th style="width: 11.11%; text-align: center;">Municipio</th>
                                                        <th style="width: 11.11%; text-align: center;">Parroquia</th>
                                                        <th style="width: 11.11%; text-align: center;">Geografico</th>
                                                        <th style="width: 11.11%; text-align: center;">Sector</th>
                                                        <th style="width: 11.11%; text-align: center;">Urbanizacion</th>

                                                    </tr>
                                                </thead>
                                            </table>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 37px;">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style type="text/css">
        .well2{

            background: #f9f9f9;
            cursor: pointer;
            /*margin-left: 30px;*/
            margin-bottom: 10px;
            padding: 8px;
        };
        .b{
            padding: 12px;
        };

    </style>
@endpush
@push('js')
<script type="text/javascript">
var $tabla,$tabla2,$tabla3, visto = 0, visto2 = 0, visto3 = 0;

var datatableEspanol = {
	"sProcessing":     "Procesando...",
	"sLengthMenu":     "Mostrar _MENU_ registros",
	"sZeroRecords":    "No se encontraron resultados",
	"sEmptyTable":     "Ningún dato disponible en esta tabla",
	"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	"sInfoPostFix":    "",
	"sSearch":         "Buscar:",
	"sUrl":            "",
	"sInfoThousands":  ",",
	"sLoadingRecords": "Cargando...",
	"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
};

//graficas();
info_centro('{{$info_centro->id}}', '{{$informacion_personal->mesa}}');
$('#modal_centro').on('click', function(){
    modal_centro();
    $('#exampleModalLong').modal('show');
});
$('#modal_territoriales_m').on('click', function(){
    modal_territorial();
    $('#exampleModalLong-2').modal('show');
});
$('#modal_territoriales_s').on('click', function(){
    modal_sectorial()
    $('#exampleModalLong-3').modal('show');
});



function modal_centro(){
    if(visto >= 1){
        $tabla.ajax.reload();
        return;
    }

    visto = 1;

    $tabla = $('#tabla').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: dire + '/consulta/datacentro',
            data: function (d) {
                d.ci = "{{$ci}}";
            }
        },
        language: datatableEspanol,
        columns: [
            {data: 'cedula_mi1x10', name: 'miembro1x10.cedula_mi1x10'},
            {data: 'nombres_mi1x10', name: 'miembro1x10.nombres_mi1x10'},
            {data: 'apellidos_mi1x10', name: 'miembro1x10.apellidos_mi1x10'},
            {data: 'centro_mi1x10', name: 'miembro1x10.centro_mi1x10'},
            {data: 'telefono_mi1x10', name: 'miembro1x10.telefono_mi1x10'},
            {data: 'centro', name: 'centro'}
        ]
    });

    $('#tabla').on("click", "tbody tr", function(){
         var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo2', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
    });
}

function modal_territorial(){
    if(visto2 >= 1){
        $tabla2.ajax.reload();
        return;
    }

    visto2 = 1;

    $tabla2 = $('#tabla2').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: dire + '/consulta/dataterimunicipio',
            data: function (d) {
                d.ci = "{{$pat_territoriales_tmunicipios->codigo_pat_territorial}}";
            }
        },
        language: datatableEspanol,
        columns: [
            {data: 'ced_patrulla', name: 'pat_territoriales_tmunicipios.ced_patrulla'},
            {data: 'nombre_rep', name: 'rep_bolivar.nombre_rep'},
            {data: 'des_municipio', name: 'pat_territoriales_tmunicipios.des_municipio'},
            {data: 'desp_parroquia', name: 'pat_territoriales_tmunicipios.desp_parroquia'},

        ]
    });

    $('#tabla2').on("click", "tbody tr", function(){
        var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo53', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
    });

}

function modal_sectorial(){
    if(visto3 >= 1){
        $tabla3.ajax.reload();
        return;
    }

    visto3 = 1;

    $tabla3 = $('#tabla3').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: dire + '/consulta/dataterisect',
            data: function (d) {
                d.ci = "{{$pat_sectoriales_municipios->codigo_pat_sectorial}}";
            }
        },
        language: datatableEspanol,
        columns: [
            {data: 'cedula_patrulla_sect',  name: 'cedula_patrulla_sect'},
            {data: 'NOMBRES',               name: 'NOMBRES'},
            {data: 'APELLIDOS',             name: 'APELLIDOS'},
            {data: 'TELEFONO_RESIDENCIA',   name: 'TELEFONO_RESIDENCIA'},
            {data: 'MUNICIPIO_NOMBRE',      name: 'MUNICIPIO_NOMBRE'},
            {data: 'PARROQUIA_NOMBRE',      name: 'PARROQUIA_NOMBRE'},
            {data: 'GEOGRAFICO_DES',        name: 'GEOGRAFICO_DES'},
            {data:'SECTOR',                 name: 'SECTOR'},
            {data:'URBANIZACION',           name: 'URBANIZACION'},

        ]
    });

    $('#tabla3').on("click", "tbody tr", function(){
        var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo553', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
    });

}

function graficas_centro(series, categories){
    var chart = Highcharts.chart('grafica_centro', {
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        xAxis: {
            categories: categories,
            tickmarkPlacement: 'on',
            title: {
                enabled: 'false'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}'
        },
        colors: ["#b93f3f", "#7cb5ec"],
        plotOptions: {
            bar: {

                colorByPoint: true,
                dataLabels: {
                    text: ' ',
                    enabled: true
                },
                groupPadding: 0.05
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        series: series
    });

}

function info_centro(centro, mesa){

     $.ajax({
        url: dire + '/inicio/centromesa',
        type: 'POST',
        data: {
            'centro': centro,
            'mesa': mesa,
        },
        success: function(r) {
           graficas_centro(r.series, r.categories);
        }
    });

}


</script>
@endpush
