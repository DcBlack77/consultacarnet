@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Rep']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Rep.',
        'columnas' => [
        'Nacionalidad' => '5.5555555555556',
		'Dni' => '5.5555555555556',
		'Nombre' => '5.5555555555556',
		'Genero' => '5.5555555555556',
		'Nacimiento' => '5.5555555555556',
		'Centro' => '5.5555555555556',
		'Municipio' => '5.5555555555556',
		'Parroquia' => '5.5555555555556',
		'Direccion' => '5.5555555555556',
		'Movil' => '5.5555555555556',
		'Telefono' => '5.5555555555556',
		'Tlf' => '5.5555555555556',
		'Fch' => '5.5555555555556',
		'Fcm' => '5.5555555555556',
		'Cp' => '5.5555555555556',
		'Psuv' => '5.5555555555556',
		'Gob' => '5.5555555555556',
		'Fd' => '5.5555555555556'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Rep->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection