var adonde_estoy = 1; // 1 = tendencia, 2 = torta
var $result = '';
var $municipios = "";
var $parroquias = "";
var $busqueda = "";
var $_adonde = "";
var $tipo = "";
var $_centro = false;
var $centro_id = '';
$(function() {
    $tipo = "5";

    $('.well-menu').on('click', function() {
        $tipo = this.getAttribute('data-tipo');
        graficas();
    });

    $('#bus').on('click', function() {
        if ($('#buscar').css('display') != 'none') {
            $('#buscar').css('display', 'none');
        } else {
            $('#buscar').css('display', 'block');
        }
    });
    $('.tabla_mesa').live('click', function() {

        exportar_excel($centro_id, 'mesa', this.getAttribute('data-mesa'));

    });
    $('.tabla_electores').live('click', function() {

        exportar_excel($centro_id, 'mesa', this.getAttribute('data-mesa'));

    });
    $('.tabla_psuv').live('click', function() {

        exportar_excel($centro_id, 'mesa-militante', this.getAttribute('data-mesa'));

    });
    $('.tabla_firmantes').live('click', function() {

        exportar_excel($centro_id, 'mesa-firmantes', this.getAttribute('data-mesa'));

    });
    $('.tabla_carnet').live('click', function() {

        exportar_excel($centro_id, 'mesa-carnet', this.getAttribute('data-mesa'));

    });

    $('#_electores').on('click', function() {
        exportar_excel($centro_id, 'electores', 0);
    });
    $('#_militante').on('click', function() {
        exportar_excel($centro_id, 'militante', 0);
    });
    $('#_firmantes').on('click', function() {
        exportar_excel($centro_id, 'firmantes', 0);
    });

    $('#_carnet').on('click', function() {
        exportar_excel($centro_id, 'carnet', 0);
    });

    tabla_comparativa('municipios', '');

    $('#tendencia').on('click', function() {
        adonde_estoy = 1;

        graficas();

    }).click();

    $('#torta').on('click', function() {

        adonde_estoy = 2;

        graficas();

    });

    $(".municipios").live('click', function() {

        tabla_comparativa('parroquias', this.getAttribute('data-id'));

        $('#regresar').css('display', 'block');

        $municipios = this.getAttribute('data-id');

        $('.nombre_municipio').html('');
        $('.nombre_municipio').append('- ' + $municipios);

        $('#adonde').val('municipios');
        $('#exampleModalLong2').modal('hide');
        graficas();
    });

    $(".parroquias").live('click', function() {
        tabla_comparativa('centros', this.getAttribute('data-id'));
        $parroquias = this.getAttribute('data-id');
        $('.nombre_parroquia').append('' + $parroquias);
        $('#adonde').val('parroquias');
        $_centro = true;
        graficas();
    });
    
    $(".centros").live('click', function() {
        $centro_id = this.getAttribute('data-id');
        info_centro(this.getAttribute('data-id'));
        $('#exampleModalLong').modal('show');
    });
    $("#comparar").live('click', function() {


        $('#exampleModalLong2').modal('show');

    });

    $('#comparar2').on('click', function() {
        tabla_comprar($('#elecciones_1').val(), $('#elecciones_2').val());
    });

    $('#regresar').on('click', function() {

        if ($('#adonde').val() == 'municipios') {
            $('.nombre_municipio').html("");
            $('#regresar').css('display', 'none');
            tabla_comparativa('municipios', '');
            $municipios = '';
            $('#adonde').val('');
        }

        if ($('#adonde').val() == 'parroquias') {
            tabla_comparativa('parroquias', $municipios);
            $parroquias = '';
            $('#adonde').val('municipios');
            $_centro = false; //variable para cambiar conducta data_id en centros
            $('.nombre_parroquia').html("");
        }

        graficas();
    });

    $('#boton_busqueda').on('click', function() {
        if ($('#ci').val() != '') {
            datos_personas($('#ci').val());
        }
    });

    $("#ci").keyup(function(event) {
        if (event.which == 13) {
            if ($('#ci').val() != '') {
                datos_personas($('#ci').val());
            }
        }
    });
});


function grafica_tendencia($tipo, nombre, adonde) {

    $quien_soy = quien_soy($tipo);

    $('.tipo_elecciones').html('');
    $('.tipo_elecciones').append($quien_soy);
    $.ajax({
        url: $url + 'grafica',
        type: 'POST',
        data: {
            'tipo': $tipo,
            'nombre': nombre,
            'adonde': adonde
        },
        success: function(r) {

            var chart = Highcharts.chart('grafica_principal', {

                title: {
                    text: 'Comportamiento Elecciones ' + $quien_soy + ' en el Estado Bolívar'
                }, 

                subtitle: {
                    text: 'Fuente: Sala Situacional'
                },

                xAxis: {
                    categories: r.categories,
                    tickmarkPlacement: 'on',
                    title: {
                        enabled: 'false'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}'
                },
                colors: ["#b93f3f", "#7cb5ec"],
                plotOptions: {
                    bar: {

                        colorByPoint: true,
                        dataLabels: {
                            text: ' ',
                            enabled: true
                        },
                        groupPadding: 0.05
                    },
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
                series: r.series
            });


        }
    });

}

function grafica_torta($tipo, nombre, adonde) {
    $quien_soy = quien_soy($tipo);

    $('.tipo_elecciones').html('');
    $('.tipo_elecciones').append($quien_soy);

    $.ajax({
        url: $url + 'graficatorta',
        type: 'POST',
        data: {
            'tipo': $tipo,
            'nombre': nombre,
            'adonde': adonde
        },
        success: function(r) {
            var chart = Highcharts.chart('grafica_principal', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Comportamiento Elecciones ' + $quien_soy + ' en el Estado Bolívar'
                },
                subtitle: {
                    text: 'Click en las secciones para ver el detallado. Fuente: Sala Situacional'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                },
                series: r.series,
                drilldown: r.drilldown
            });
        }
    });

}

function quien_soy(adonde) {

    switch (adonde) {
        case "1":
            $result = 'Presidenciales';
            break;
        case "2":
            $result = 'Regionales';
            break;
        case "3":
            $result = 'Municipales';
            break;
        case "4":
            $result = 'Otras Elecciones';
            break;
        case "5":
            $result = 'Ultimas 6 Elecciones';
            break;
    }
    return $result;
}

function datos_personas($ci) {
    var win = window.open($url + 'consulta/' + $ci, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
    win.focus();
}

function tabla_comparativa(adonde, tipo) {

    $.ajax({
        url: $url + adonde,
        type: 'POST',
        data: {
            'tipo': tipo
        },
        success: function(r) {
            $('#listado_municipios').html('');
            
           
            $('#poblacion').html('');
            $('#electores').html('');
            $('#militantes').html('');
            $('#centros').html('');
            $('#mesas').html('');
            $('#parroquias').html('');
            $('#poblacion').append(number_format(r.poblacion,0));
            $('#electores').append(number_format(r.electores,0));
            $('#militantes').append(number_format(r.militante,0));
            $('#centros').append(number_format(r.centros,0));
            $('#mesas').append(number_format(r.mesas,0));
            $('#parroquias').append(number_format(r.parroquias,0));
            
            
            crear_tabla(r.datos, adonde);
            
        }
    });
}
function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join(',');
}
function crear_tabla(datos, tipo) {
    var vacio = {
            votosoficiales: 0,
            votosoposicion: 0,
            resultado: 0,
        },
        elecciones = [
            'Regionales 2008',
            'Elecciones Parlamentarias 2010',
            'Presidenciales 2012',
            'Regionales 2012',
            'Presidenciales 2013',
            'Elecciones Parlamentarias 2015',
            'Elecciones Regionales 2017'
        ];

    for (var i in datos) {
        if ($_centro == true) {
            var index = datos[i].id;
        } else {
            var index = datos[i].municipios;
        }
        var tr = '<tr class="' + tipo + '"  data-id = "' + index + '" id="' + datos[i].municipios + '" style="cursor: pointer;">';
        tr += '<td>' + datos[i].municipios + '</td>';

        for (var j in elecciones) {
            var eleccion = elecciones[j],
                data = datos[i][eleccion];
            if (data == undefined) {
                data = vacio;
            }

            tr += '<td><span style="color:red;">' + data.votosoficiales + '</span> <br/>';
            tr += data.votosoposicion + '</td>';
            tr += '<td>' + data.resultado + '</td>';
        }

        tr += '</tr>';

        $('#listado_municipios').append(tr);
    }
}

function graficas() {

    if ($('#adonde').val() == 'municipios') {
        $busqueda = $municipios;
        $_adonde = 'municipio';
    }

    if ($('#adonde').val() == 'parroquias') {
        $busqueda = $parroquias;
        $_adonde = 'parroquia';
    }

    if (adonde_estoy == 1) {
        grafica_tendencia($tipo, $busqueda, $_adonde);
    } else if (adonde_estoy == 2) {
        //grafica_torta($tipo);
        grafica_torta($tipo, $busqueda, $_adonde);
    }

    $busqueda = "";
    $_adonde = "";
}

function info_centro(centro) {

    $.ajax({
        url: $url + 'infocentro',
        type: 'POST',
        data: {
            'centro': centro,
        },
        success: function(r) {

            _limpiar();

            $('.nombre_centro').append(r.info_centro[0].nombre);
            $('#codigo').append(r.info_centro[0].id);
            $('#direccion').append(r.info_centro[0].direccion);
            /* $('#mesas').append(r.info_centro[0].mesas);*/
            $('#electores').append(r.info_centro[0].votantes);

            $('#firmantes').append(r.firmantes[0].total);

            if (r.militantes[0].total == undefined) {
                var mili = 0;
            } else {
                var mili = r.militantes[0].total;
            }
            $('#militante').append(mili);
            $('#carnet').append(r.carnet);

            /* $('#firmantes').append(r.info_centro[0].);*/

            tabla_centros(r.datos);
            mesas_electorales(r.mesas);
            graficas_centro(r.series, r.categories);
        }
    });

}

function _limpiar() {
    $('.nombre_centro').html('');
    $('#codigo').html('');
    $('#direccion').html('');
    $('#mesas').html('');
    $('#electores').html('');
    $('#militante').html('');
    $('#firmantes').html('');
    $('#carnet').html('');
}

function tabla_centros(datos) {
    $('#listado_mesas').html('');
    var vacio = {
            votosoficiales: 0,
            votosoposicion: 0,
            resultado: 0,
        },
        elecciones = [
            'Regionales 2008',
            'Elecciones Parlamentarias 2010',
            'Presidenciales 2012',
            'Regionales 2012',
            'Presidenciales 2013',
            'Elecciones Parlamentarias 2015',
            'Elecciones Regionales 2017'
        ];

    for (var i in datos) {

        var tr = '<tr class=""  data-id = "" id="" style="cursor: pointer;">';
        tr += '<td>' + datos[i].mesa + '</td>';

        for (var j in elecciones) {
            var eleccion = elecciones[j],
                data = datos[i][eleccion];
            if (data == undefined) {
                data = vacio;
            }

            tr += '<td><span style="color:red;">' + data.votosoficiales + '</span> <br/>';
            tr += data.votosoposicion + '</td>';
            tr += '<td>' + data.resultado + '</td>';
        }

        tr += '</tr>';

        $('#listado_mesas').append(tr);
    }
}

function mesas_electorales(datos) {

    $('#mesas_electorales').html('');

    for (var i in datos) {

        if (datos[i].mesa == undefined) {
            continue;
        }
        $('#mesas_electorales').append(
            '<tr><td class="tabla_mesa" data-mesa=' + datos[i].mesa + '>' + datos[i].mesa +
            '</td><td class="tabla_electores" data-mesa=' + datos[i].mesa + '>' + datos[i].electores +
            '</td><td class="tabla_psuv" data-mesa=' + datos[i].mesa + ' >' + datos[i].psuv +
            '</td><td class="tabla_firmantes" data-mesa=' + datos[i].mesa + '>' + datos[i].firmantes +
            '</td><td class="tabla_carnet" data-mesa=' + datos[i].mesa + '>' + datos[i].carnet +
            '</td></tr>'
        );
    }


}

function graficas_centro(series, categories) {
    var chart = Highcharts.chart('grafica_centro', {
        title: {
            text: 'Comportamiento Elecciones  en el Estado Bolívar'
        },

        subtitle: {
            text: 'Fuente: Sala Situacional'
        },

        xAxis: {
            categories: categories,
            tickmarkPlacement: 'on',
            title: {
                enabled: 'false'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}'
        },
        colors: ["#b93f3f", "#7cb5ec"],
        plotOptions: {
            bar: {

                colorByPoint: true,
                dataLabels: {
                    text: ' ',
                    enabled: true
                },
                groupPadding: 0.05
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        series: series
    });

}

function tabla_comprar($elecciones_1, $elecciones_2) {

    $.ajax({
        url: $url + 'comparar',
        type: 'POST',
        data: {
            'elecciones_1': $elecciones_1,
            'elecciones_2': $elecciones_2
        },
        success: function(r) {
            // $('#listado_municipios').html('');

            console.log(r.datos);
            crear_tabla_3(r.datos, r.elecciones);
        }
    });

}

function crear_tabla_3(datos, elecciones) {
    $('#listado_municipios_2').html('');
    var vacio = {
        votosoficiales: 0,
        votosoposicion: 0,
        resultado: 0,
    };

    for (var i in datos) {

        var index = datos[i].municipios;

        var tr = '<tr class="municipios "  data-id = "' + index + '" id="' + datos[i].municipios + '" style="cursor: pointer;">';
        tr += '<td>' + datos[i].municipios + '</td>';

        for (var j in elecciones) {
            var eleccion = elecciones[j],
                data = datos[i][eleccion];
            if (data == undefined) {
                data = vacio;
            }

            tr += '<td><span style="color:red;">' + data.votosoficiales + '</span> <br/>';
            tr += data.votosoposicion + '</td>';
            tr += '<td>' + data.resultado + '</td>';
        }

        tr += '</tr>';

        $('#listado_municipios_2').append(tr);
    }
}

function exportar_excel(centro_id, quienes, mesas) {

    ruta = dire + '/inicio/excel/' + centro_id + '/' + quienes + '/' + mesas
    window.open(ruta, '_blank');

}