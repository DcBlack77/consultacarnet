var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"nacionalidad","name":"nacionalidad"},{"data":"dni","name":"dni"},{"data":"nombre","name":"nombre"},{"data":"genero","name":"genero"},{"data":"nacimiento","name":"nacimiento"},{"data":"centro_id","name":"centro_id"},{"data":"municipio","name":"municipio"},{"data":"parroquia","name":"parroquia"},{"data":"direccion","name":"direccion"},{"data":"movil","name":"movil"},{"data":"telefono","name":"telefono"},{"data":"tlf","name":"tlf"},{"data":"fch","name":"fch"},{"data":"fcm","name":"fcm"},{"data":"cp","name":"cp"},{"data":"psuv","name":"psuv"},{"data":"gob","name":"gob"},{"data":"fd","name":"fd"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});