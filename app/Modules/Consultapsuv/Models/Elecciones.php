<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;



class Elecciones extends Modelo
{
    protected $table = 'elecciones';
    protected $fillable = ["nombre","fecha","tipo"];
   

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

}