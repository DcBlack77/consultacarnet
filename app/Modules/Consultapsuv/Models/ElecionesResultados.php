<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;


class ElecionesResultados extends Modelo
{
    protected $table = 'eleciones_resultados';
    protected $fillable = ["elecciones_id","centros_id","mesa","oficialsmo","oposicion","total_electores","participacion","abstencion","nulos"];
    
}