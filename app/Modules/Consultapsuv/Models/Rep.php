<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;

class Rep extends Modelo
{
    protected $table = 'rep';
    protected $fillable = [
        'nacionalidad',
        'dni',
        'nombre',
        'genero',
        'nacimiento',
        'centro_id',
        'municipio',
        'parroquia',
        'direccion',
        'movil',
        'telefono',
        'tlf',
        'fch',
        'fcm',
        'cp',
        'psuv',
        'unopordiez',
        'ubch',
        'clp',
        'lider_calle',
        'gob',
        'fd',
        'alcaldia',
        'fondobolivar',
    ];
    protected $campos = [
        'nacionalidad' => [
            'type' => 'text',
            'label' => 'Nacionalidad',
            'placeholder' => 'Nacionalidad del Rep'
        ],
        'dni' => [
            'type' => 'number',
            'label' => 'Dni',
            'placeholder' => 'Dni del Rep'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Rep'
        ],
        'genero' => [
            'type' => 'text',
            'label' => 'Genero',
            'placeholder' => 'Genero del Rep'
        ],
        'nacimiento' => [
            'type' => 'date',
            'label' => 'Nacimiento',
            'placeholder' => 'Nacimiento del Rep'
        ],
        'centro_id' => [
            'type' => 'number',
            'label' => 'Centro',
            'placeholder' => 'Centro del Rep'
        ],
        'municipio' => [
            'type' => 'text',
            'label' => 'Municipio',
            'placeholder' => 'Municipio del Rep'
        ],
        'parroquia' => [
            'type' => 'text',
            'label' => 'Parroquia',
            'placeholder' => 'Parroquia del Rep'
        ],
        'direccion' => [
            'type' => 'text',
            'label' => 'Direccion',
            'placeholder' => 'Direccion del Rep'
        ],
        'movil' => [
            'type' => 'text',
            'label' => 'Movil',
            'placeholder' => 'Movil del Rep'
        ],
        'telefono' => [
            'type' => 'text',
            'label' => 'Telefono',
            'placeholder' => 'Telefono del Rep'
        ],
        'tlf' => [
            'type' => 'text',
            'label' => 'Tlf',
            'placeholder' => 'Tlf del Rep'
        ],
        'fch' => [
            'type' => 'checkbox',
            'label' => 'Fch',
            'placeholder' => 'Fch del Rep'
        ],
        'fcm' => [
            'type' => 'checkbox',
            'label' => 'Fcm',
            'placeholder' => 'Fcm del Rep'
        ],
        'cp' => [
            'type' => 'checkbox',
            'label' => 'Cp',
            'placeholder' => 'Cp del Rep'
        ],
        'psuv' => [
            'type' => 'checkbox',
            'label' => 'Psuv',
            'placeholder' => 'Psuv del Rep'
        ],
        'gob' => [
            'type' => 'checkbox',
            'label' => 'Gob',
            'placeholder' => 'Gob del Rep'
        ],
        'fd' => [
            'type' => 'checkbox',
            'label' => 'Fd',
            'placeholder' => 'Fd del Rep'
        ]
    ];
}