<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class RepBolivar extends Model
{
    protected $table = 'rep_bolivar';
    protected $fillable = ["cedula","nombre_rep","fecha_nacimiento","sexo","cod_centro","mesa","psuv","sectorial","territorial","ubch","circuito"];
   

    
}