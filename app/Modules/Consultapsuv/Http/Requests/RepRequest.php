<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class RepRequest extends Request {
    protected $reglasArr = [
		'nacionalidad' => ['required', 'min:3', 'max:1'], 
		'dni' => ['required', 'integer'], 
		'nombre' => ['min:3', 'max:255'], 
		'genero' => ['required', 'min:3', 'max:1'], 
		'nacimiento' => ['date_format:"d/m/Y"'], 
		'centro_id' => ['integer'], 
		'municipio' => ['min:3', 'max:255'], 
		'parroquia' => ['min:3', 'max:255'], 
		'direccion' => ['min:3', 'max:255'], 
		'movil' => ['min:3', 'max:50'], 
		'telefono' => ['min:3', 'max:50'], 
		'tlf' => ['min:3', 'max:50'], 
		'fch' => ['required'], 
		'fcm' => ['required'], 
		'cp' => ['required'], 
		'psuv' => ['required'], 
		'gob' => ['required'], 
		'fd' => ['required']
	];
}