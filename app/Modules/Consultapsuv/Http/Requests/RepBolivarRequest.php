<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class RepBolivarRequest extends Request {
    protected $reglasArr = [
		'cedula' => ['required', 'min:3', 'max:100', 'unique:rep_bolivar,cedula'], 
		'nombre_rep' => ['min:3', 'max:200'], 
		'fecha_nacimiento' => ['min:3', 'max:100'], 
		'sexo' => ['min:3', 'max:100'], 
		'cod_centro' => ['min:3', 'max:100'], 
		'mesa' => ['min:3', 'max:100'], 
		'psuv' => ['min:3', 'max:100'], 
		'sectorial' => ['min:3', 'max:100'], 
		'territorial' => ['min:3', 'max:100'], 
		'ubch' => ['min:3', 'max:100'], 
		'circuito' => ['min:3', 'max:100']
	];
}