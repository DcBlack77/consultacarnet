<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class FirmantesRevRequest extends Request {
    protected $reglasArr = [
		'id_firma_maduro' => ['min:3', 'max:100'], 
		'cedula_firmantes' => ['min:3', 'max:100'], 
		'firmo_revocatorio' => ['min:3', 'max:100'], 
		'voto_6d' => ['min:3', 'max:100'], 
		'trabaja' => ['min:3', 'max:100']
	];
}