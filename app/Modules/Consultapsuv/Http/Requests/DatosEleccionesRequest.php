<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class DatosEleccionesRequest extends Request {
    protected $reglasArr = [
		'descrip_eleccion' => ['required', 'min:3', 'max:100'], 
		'fecano' => ['required', 'min:3', 'max:6'], 
		'sta_elecciones' => ['required', 'integer']
	];
}