<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class ElecionesResultadosRequest extends Request {
    protected $reglasArr = [
		'elecciones_id' => ['required', 'integer'], 
		'centros_id' => ['required', 'integer'], 
		'mesa' => ['required'], 
		'oficialsmo' => ['required', 'integer'], 
		'oposicion' => ['required', 'integer'], 
		'total_electores' => ['required', 'integer'], 
		'participacion' => ['required', 'integer'], 
		'abstencion' => ['required', 'integer'], 
		'nulos' => ['required', 'integer']
	];
}