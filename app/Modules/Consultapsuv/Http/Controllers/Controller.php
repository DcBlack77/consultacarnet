<?php

namespace App\Modules\Consultapsuv\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'Consultapsuv';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Consultapsuv/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Consultapsuv/Assets/css',
	];

	public $libreriasIniciales = [
		'OpenSans',
		'font-awesome',
		'simple-line-icons',
		'jquery-easing',
		'jquery-migrate',
		'animate',
		'bootstrap',
		'bootbox',
	
		'pace',
		'jquery-form',
		'blockUI',
		'jquery-shortcuts',
		'pnotify',
		'bootstrap-hover-dropdown',
	
		'jquery-ui',
		'owl-carousel',
		'plantilla',
		'mousewheel',
		'retina',
		'scrollUp',
		'headroom',
		'sticky-kit',
		'scroll',
		'flexslider',
		'animate',
		'custom',
		'bootstrap-social'
	];

}