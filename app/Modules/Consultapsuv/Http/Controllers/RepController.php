<?php

namespace App\Modules\Consultapsuv\Http\Controllers;

//Controlador Padre
use App\Modules\Consultapsuv\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Consultapsuv\Http\Requests\RepRequest;

//Modelos
use App\Modules\Consultapsuv\Models\Rep;

class RepController extends Controller
{
    protected $titulo = 'Rep';

    public $js = [
        'Rep'
    ];
    
    public $css = [
        'Rep'
    ];

    public $librerias = [
        'datatables'
    ];

    public $libreriasIniciales = [
        'OpenSans',
        'font-awesome',
        'simple-line-icons',
        'jquery-easing',
        'jquery-migrate',
        'animate',
        'bootstrap',
        'bootbox',
        //'jquery-cookie'
        'pace',
        'jquery-form',
        'blockUI',
        'jquery-shortcuts',
        'pnotify',
        'metronic',
        'bootstrap-hover-dropdown',
        //'jquery-slimscroll',
    ];

    public function index()
    {
        return $this->view('consultapsuv::Rep', [
            'Rep' => new Rep()
        ]);
    }

    public function nuevo()
    {
        $Rep = new Rep();
        return $this->view('consultapsuv::Rep', [
            'layouts' => 'base::layouts.popup',
            'Rep' => $Rep
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Rep = Rep::find($id);
        return $this->view('consultapsuv::Rep', [
            'layouts' => 'base::layouts.popup',
            'Rep' => $Rep
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Rep = Rep::withTrashed()->find($id);
        } else {
            $Rep = Rep::find($id);
        }

        if ($Rep) {
            return array_merge($Rep->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(RepRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Rep = $id == 0 ? new Rep() : Rep::find($id);

            $Rep->fill($request->all());
            $Rep->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Rep->id,
            'texto' => $Rep->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Rep::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Rep::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Rep::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Rep::select([
            'id', 'nacionalidad', 'dni', 'nombre', 'genero', 'nacimiento', 'centro_id', 'municipio', 'parroquia', 'direccion', 'movil', 'telefono', 'tlf', 'fch', 'fcm', 'cp', 'psuv', 'gob', 'fd', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}