<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class import extends Command
{
    protected $signature = 'import {paso?} {--dev}';

    protected $description = 'Import de archivos excel';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->import = new importClass();
        $this->import->console = $this;
        $this->import->dev = $this->option('dev');
        
        $paso = $this->argument('paso');
        return $this->import->init($paso);
        
    }

    public function getOutput()
    {
        return $this->output;
    }
}